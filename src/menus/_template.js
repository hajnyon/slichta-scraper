exports.templatePageFunction = async (context, Apify) => {
    const { page, request } = context;

    // date
    const menuDate = '';

    const menuData = {
        soups: [],
        meals: []
    };

    // soups
    menuData.soups.push({});

    // meals
    menuData.meals.push({});

    Apify.utils.log.info(`URL scraped.`, { url: request.url });

    await Apify.pushData({
        url: request.url,
        menus: [
            {
                date: menuDate,
                data: menuData
            }
        ]
    });
};
