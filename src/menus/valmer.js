const { milliSecondsFromDateString, numberFromString } = require('../helpers');

const NAME = 'Valmer';
const DATE_REGEX = /\d+.\s\d+.\s\d+/g;
exports.valmerPageFunction = async (data) => {
    const { id, context, Apify } = data;
    const { page, request } = context;

    // date
    const menuDateData = await page.$eval('.menu_book .meal_menu .date', (el) => el.textContent);
    const dateString = DATE_REGEX.exec(menuDateData)[0] ?? null;
    if (!dateString) {
        throw new Error(`Can't get date.`);
    }
    const menuDate = milliSecondsFromDateString(dateString);

    const menuData = {
        soups: [],
        meals: []
    };
    const mealsSections = await page.$$('.menu_book .meal_menu .meals_wrap');
    for (const section of mealsSections) {
        const sectionHeading = await section.$eval('h3', (el) => el.textContent);

        const mealsRows = await section.$$('.meal');

        const meals = [];
        for (const row of mealsRows) {
            const name = await row.$eval('.name', (el) => el.textContent);
            const priceData = await row.$eval('.price', (el) => el.textContent);
            const price = priceData === '' ? null : numberFromString(priceData);
            const tags = [];

            if (sectionHeading === 'Rozvoz') {
                tags.push('delivery');
            } else if (sectionHeading === 'Dezert') {
                tags.push('desert');
            } else if (sectionHeading === 'Zvýhodněné menu') {
                tags.push('discount');
            }

            meals.push({
                price: price,
                name: name,
                tags: tags
            });
        }

        if (sectionHeading === 'Polévka') {
            menuData.soups.push(...meals);
        } else {
            menuData.meals.push(...meals);
        }
    }

    Apify.utils.log.info(`URL scraped.`, { url: request.url });

    const menus =
        menuData.meals.length > 0
            ? [
                  {
                      date: menuDate,
                      data: menuData
                  }
              ]
            : [];

    await Apify.pushData({
        id: id,
        url: request.url,
        name: NAME,
        menus: menus
    });
};
