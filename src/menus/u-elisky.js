const { milliSecondsFromDateString, numberFromString } = require('../helpers');

const NAME = 'U Elišky';
const DATE_REGEX = /\d+\.\s?\d+\.\s?\d+/;
const WEIGHT_REGEX = /(?<grams>\d+)\s?\g/;

exports.uEliskyPageFunction = async (data) => {
    const { id, context, Apify } = data;
    const { page, request } = context;

    const menus = [];
    const rows = await page.$$('.standard_section');
    for (const row of rows) {
        const menuData = {
            soups: [],
            meals: []
        };
        // date
        const headers = await row.$$('h3');
        if (headers[0] === undefined) {
            console.error(`Can't get header.`);
            continue;
        }
        const menuDateData = await row.$eval('h3', (el) => el.textContent);
        if (!DATE_REGEX.test(menuDateData)) {
            console.error(`Can't get date.`);
            continue;
        }
        const menuDate = milliSecondsFromDateString(menuDateData);

        const meals = await row.$$('.nectar_food_menu_item');

        for (const meal of meals) {
            let mealData = await meal.$eval('.item_name', (el) => el.textContent);
            let mealPrize = null;
            const mealPrizeItem = await meal.$('.item_price');
            if (mealPrizeItem === null) {
                console.error(e);
            } else {
                const mealPrizeData = await meal.$eval('.item_price', (el) => el.textContent);
                mealPrize = numberFromString(mealPrizeData);
            }

            let soup = false;
            const weightSearch = WEIGHT_REGEX.exec(mealData);
            let weightData = undefined;
            if (weightSearch === null) {
                soup = true;
            } else {
                weightData = Number(weightSearch.groups.grams);
                mealData = mealData.replace(WEIGHT_REGEX, '');
            }

            const mealResult = {
                price: mealPrize,
                name: mealData,
                weight: weightData,
                tags: []
            };
            if (soup) {
                menuData.soups.push(mealResult);
            } else {
                menuData.meals.push(mealResult);
            }
        }

        menus.push({
            date: menuDate,
            data: menuData
        });
    }
    Apify.utils.log.info(`URL scraped.`, { url: request.url });

    await Apify.pushData({
        id: id,
        url: request.url,
        name: NAME,
        menus: menus
    });
};
