const { trimLeadingComma, numberFromString, milliSecondsFromDateString } = require('../helpers');

const NAME = 'U Flíčků';
const DATE_REGEX = /\d+.\d+.\d+/g;

exports.uFlickuPageFunction = async (data) => {
    const { id, context, Apify } = data;
    const { page, request } = context;

    // check if menu exists for the day
    const noMenu = await page.$('.zadne-menu');
    if (noMenu !== null) {
        await Apify.pushData({
            id: id,
            url: request.url,
            name: NAME,
            menus: []
        });
        return;
    }

    // date
    const menuDateData = await page.$eval('.menu-datum h2', (el) => el.textContent);
    const dateString = DATE_REGEX.exec(menuDateData)[0] ?? null;
    if (!dateString) {
        throw new Error(`Can't get date.`);
    }
    const menuDate = milliSecondsFromDateString(dateString);

    const menuData = {
        soups: [],
        meals: []
    };

    // soups
    const soupRows = await (await page.$('.obedy-menu.dnes')).$$('tr.polevka');
    for (const soupRow of soupRows) {
        const nameData = await soupRow.$eval('td.polevkatd', (el) => el.textContent);
        const name = nameData.replace('Polévka: ', '');
        const priceData = await soupRow.$eval('td.polevka-cena', (el) => el.textContent);
        const price = numberFromString(priceData);
        const soupData = {
            price: price,
            name: name,
            tags: []
        };
        menuData.soups.push(soupData);
    }

    // meals
    const mealRows = await (await page.$('.obedy-menu.dnes')).$$('tr:not(.polevka):not(.alergeny)');
    const getData = async (data, key, selector, elem) => {
        try {
            data[key] = await elem.$eval(selector, (el) => el.textContent);
            if (key === 'sideDish') {
                data[key] = trimLeadingComma(data[key]);
            }
        } catch (error) {
            data[key] = null;
        }
    };

    for (const mealRow of mealRows) {
        const priceData = await mealRow.$eval('td.cena', (el) => el.textContent);
        const weightData = await mealRow.$eval('td.gramaz', (el) => el.textContent);
        const mealData = await mealRow.$('td.jidlo');
        let meal = {
            tags: []
        };
        await getData(meal, 'name', '.jidlo-span', mealData);
        await getData(meal, 'sideDish', '.priloha-span', mealData);
        await getData(meal, 'description', '.popis', mealData);
        const rowData = {
            price: numberFromString(priceData),
            weight: numberFromString(weightData),
            ...meal
        };
        menuData.meals.push(rowData);
    }

    Apify.utils.log.info(`URL scraped.`, { url: request.url });

    await Apify.pushData({
        id: id,
        url: request.url,
        name: NAME,
        menus: [
            {
                date: menuDate,
                data: menuData
            }
        ]
    });
};
