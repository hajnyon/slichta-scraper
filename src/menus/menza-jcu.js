const { numberFromString, milliSecondsFromDateString } = require('../helpers');

const NAME = 'Menza JČU';

const KEY_LUNCH = 'Oběd - ';
const KEY_FAST = 'Minutka - ';
const KEY_SOUP = 'Polévka - ';

async function getMenus(page, key) {
    let type = null;
    const menus = new Map();
    let menu = null;

    const rows = await page.$$('#Jidelnicek1__TableJidelnicek > tbody tr');
    for (const row of rows) {
        const className = await (await row.getProperty('className')).jsonValue();
        if (className === '' || className === null) {
            const dateText = await row.$eval('td', (el) => el.textContent);
            if (dateText.indexOf(key) > -1) {
                const date = milliSecondsFromDateString(dateText.replace(key, ''));
                type = 'meals';
                menu = {
                    date: date,
                    data: { soups: [], meals: [] }
                };
            } else if (dateText.indexOf(KEY_SOUP) > -1) {
                type = 'soups';
            } else {
                type = null;
            }
        }
        if (type === null) {
            if (menu !== null) {
                menus.set(menu.date, menu.data);
                menu = null;
            }
            continue;
        }
        if ((className.indexOf('TrPolozkaSeda') > -1 || className.indexOf('TrPolozka') > -1) && menu !== null) {
            const name = await row.$eval('td.TdAltNazev', (el) => el.textContent);
            const prices = await row.$$eval('td.TdCena, td.TdCenaSeda', (priceElements) => priceElements.map((el) => el.textContent));
            // const studentPrice = prices[0];
            const defaultPrice = numberFromString(prices[1]);
            // remove duplicates
            if (menu.data[type].find((value) => value.name === name)) {
                continue;
            }
            menu.data[type].push({
                price: defaultPrice,
                name: name,
                tags: key === KEY_FAST ? ['fastFood'] : []
            });
        }
    }

    return menus;
}

exports.menzaJcuPageFunction = async (data) => {
    const { id, context, Apify } = data;
    const { page, request } = context;

    // display all avaialable menus
    await page.click('#Calendar1__CheckBoxPouzeJedenDen');
    await Promise.all([page.waitForNavigation(), page.click('#_ButtonSestavJidelnicek')]);

    // scrape default meals
    const defaultMenus = await getMenus(page, KEY_LUNCH);
    Apify.utils.log.info(`Default meals scraped.`, { url: request.url });

    // switch to fast food menu
    await Promise.all([page.waitForNavigation(), page.select('#VydejnyComboBox1__DropDownListVydejny', '2')]);

    // scrape fast food meals
    const fastFoodMenus = await getMenus(page, KEY_FAST);
    Apify.utils.log.info(`Fast food meals scraped.`, { url: request.url });

    // merge meals
    const menus = [];
    for (const [date, defaultData] of defaultMenus.entries()) {
        const fastFoodData = fastFoodMenus.get(date);
        if (!fastFoodData) {
            Apify.utils.log.info(`No record for fast food on date ${date}`);
        }
        const menu = {
            date: date,
            data: {
                soups: [...defaultData.soups],
                meals: [...defaultData.meals, ...(fastFoodData ? fastFoodData.meals : [])]
            }
        };
        menus.push(menu);
    }

    Apify.utils.log.info(`URL scraped.`, { url: request.url });

    await Apify.pushData({
        id: id,
        name: NAME,
        url: request.url,
        menus: menus
    });
};
