exports.trimLeadingComma = (value) => {
    return value.replace(/^\,\s/, '');
};

exports.numberFromString = (num) => {
    const strNum = /\d+((.|\,)\d+)?/g.exec(num)[0] ?? null;
    return strNum === null ? null : Number(strNum.replace(',', '.'));
};

exports.milliSecondsFromDateString = (dateString) => {
    const {
        groups: { day, month, year }
    } = /(?<day>\d+).\s?(?<month>\d+).\s?(?<year>\d+)/.exec(dateString);
    if (!day || !month || !year) {
        throw new Error(`Can't parse date from ${dateString}`);
    }
    return new Date(year, month - 1, day).getTime();
};
