const { menzaJcuPageFunction } = require('./menus/menza-jcu');
const { uFlickuPageFunction } = require('./menus/u-flicku');
const { valmerPageFunction } = require('./menus/valmer');
const { uEliskyPageFunction } = require('./menus/u-elisky');

const restaurants = {
    'valmer': {
        id: 'valmer',
        pageFunction: valmerPageFunction
    },
    'uflicku': {
        id: 'u-flicku',
        pageFunction: uFlickuPageFunction
    },
    'menza.jcu': {
        id: 'menza-jcu',
        pageFunction: menzaJcuPageFunction
    },
    'hospudkaeliska': {
        id: 'u-elisky',
        pageFunction: uEliskyPageFunction
    }
};

exports.getRestaurant = (url) => {
    for (const restaurantKey of Object.keys(restaurants)) {
        if (url.indexOf(restaurantKey) > -1) {
            return restaurants[restaurantKey];
        }
    }
    return null;
};
