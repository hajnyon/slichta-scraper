/**
 * This template is a production ready boilerplate for developing with `PuppeteerCrawler`.
 * Use this to bootstrap your projects using the most up-to-date code.
 * If you're looking for examples or want to learn more, see README.
 */

const Apify = require('apify');
const { getRestaurant } = require('./src/restaurants');

const {
    utils: { log }
} = Apify;

Apify.main(async () => {
    const { startUrls } = await Apify.getInput();

    log.info(`Start urls.`, { startUrls });

    const requestList = await Apify.openRequestList('start-urls', startUrls);
    const requestQueue = await Apify.openRequestQueue();

    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        launchContext: {
            // Chrome with stealth should work for most websites.
            // If it doesn't, feel free to remove this.
            useChrome: true,
            stealth: true
        },
        handlePageFunction: async (context) => {
            const { url } = context.request;
            log.info('Page opened.', { url });
            const restaurant = getRestaurant(url);
            if (restaurant) {
                return restaurant.pageFunction({ id: restaurant.id, context: context, Apify: Apify });
            } else {
                log.warning(`Page has not yet it's page function setup.`, { url });
                return;
            }
        }
    });

    log.info('Starting the crawl.');
    await crawler.run();
    log.info('Crawl finished.');
});
