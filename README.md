# slichta-scraper

Scraper that obtains menus from various restaurants.

## Development

Before you start, you need to set `apify_storage/default/INPUT.json` with initial data.

```bash
npm ci
npm start
```
